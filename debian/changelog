libio-all-perl (0.87-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:06:40 +0000

libio-all-perl (0.87-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Chris Butler from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Import upstream version 0.87
  * Declare compliance with Debian policy 4.1.1

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 12 Nov 2017 21:54:59 +0100

libio-all-perl (0.86-2) unstable; urgency=medium

  * Patch in a switch to use installed IO::All when running under
    autopkgtest. (Closes: #785029)
  * Add libfile-copy-recursive-perl to Build-Depends-Indep to be able to
    run all tests in t/copy.t.

 -- Axel Beckert <abe@debian.org>  Thu, 14 May 2015 16:43:53 +0200

libio-all-perl (0.86-1) unstable; urgency=medium

  * Add debian/upstream/metadata
  * Imported upstream version 0.86
  * Declare compliance with Debian Policy 3.9.6 (no further changes)
  * Add "Testsuite: autopkgtest-pkg-perl" header.

 -- Axel Beckert <abe@debian.org>  Sun, 26 Apr 2015 04:26:50 +0200

libio-all-perl (0.79-1) unstable; urgency=medium

  * New upstream release

 -- Axel Beckert <abe@debian.org>  Wed, 10 Sep 2014 22:17:33 +0200

libio-all-perl (0.78-1) unstable; urgency=medium

  * New upstream release

 -- Axel Beckert <abe@debian.org>  Sat, 30 Aug 2014 15:26:03 +0200

libio-all-perl (0.75-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Import new upstream release

 -- Axel Beckert <abe@debian.org>  Mon, 18 Aug 2014 18:50:06 +0200

libio-all-perl (0.68-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Aug 2014 21:51:42 +0200

libio-all-perl (0.67-1) unstable; urgency=medium

  * New upstream release.
  * Update upstream copyright notice.

 -- gregor herrmann <gregoa@debian.org>  Sun, 03 Aug 2014 14:26:22 +0200

libio-all-perl (0.66-1) unstable; urgency=medium

  * New upstream release.
  * Install new documentation and examples.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Jul 2014 22:54:28 +0200

libio-all-perl (0.65-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 15 Jun 2014 14:01:28 +0200

libio-all-perl (0.63-1) unstable; urgency=medium

  * New upstream release.
  * Add build dependency on netbase to avoid warning during tests.

 -- gregor herrmann <gregoa@debian.org>  Thu, 12 Jun 2014 18:37:56 +0200

libio-all-perl (0.62-1) unstable; urgency=medium

  * Team upload.
  * Imported Upstream version 0.62

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 08 Jun 2014 11:57:49 +0200

libio-all-perl (0.61-1) unstable; urgency=medium

  * New upstream release

 -- Axel Beckert <abe@debian.org>  Fri, 09 May 2014 01:51:44 +0200

libio-all-perl (0.59-1) unstable; urgency=medium

  * New upstream release
  * Add myself to Uploaders

 -- Axel Beckert <abe@debian.org>  Sat, 08 Mar 2014 16:07:43 +0100

libio-all-perl (0.57-1) unstable; urgency=medium

  * New upstream release.
  * Update years of copyright.
  * Update Build-Depends-Indep and Suggests.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Feb 2014 21:52:15 +0100

libio-all-perl (0.54-1) unstable; urgency=low

  * Team upload
  * New upstream release

 -- Axel Beckert <abe@debian.org>  Wed, 22 Jan 2014 19:23:42 +0100

libio-all-perl (0.52-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    + io->file('…')->assert no more creates directory instead of file
      (Closes: #733680)

 -- Axel Beckert <abe@debian.org>  Thu, 16 Jan 2014 23:41:19 +0100

libio-all-perl (0.51-1) unstable; urgency=low

  * Team upload

  * New upstream release
  * Change homepage back to MetaCPAN.org URL as that one's more
    informative with regards to new upstream releases. There is also a
    link to GitHub while the GitHub page links back to search.cpan.org.
  * Bump Standards-Version to 3.9.5 (no changes)
  * Apply wrap-and-sort
  * Bump debhelper compatibility to 9
    + Gets rid of dh_pysupport deprecation warning
    + Update versioned debhelper build-dependency accordingly

 -- Axel Beckert <abe@debian.org>  Tue, 31 Dec 2013 02:40:41 +0100

libio-all-perl (0.50-1) unstable; urgency=low

  * Team upload

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Damyan Ivanov ]
  * Imported Upstream version 0.50
  * debian/copyright:
    + drop inc/Module/Install stanca, not present in the source any more
    + bump years of Ingy döt Net copyright
    + change Homepage following upstream
    + drop dirname.patch, not applicable anymore
  * claim conformance with Policy 3.9.4

 -- Damyan Ivanov <dmn@debian.org>  Sat, 19 Oct 2013 17:15:24 +0300

libio-all-perl (0.46-1) unstable; urgency=low

  * Team upload.
  * d/control: Update standards version.
  * d/copyright: Update format and field names.
  * d/copyright: Update copyright years.
  * d/copyright: Add new copyright holder Brian Ingerson.
  * New upstream release.

 -- Nuno Carvalho <smash@cpan.org>  Tue, 07 Aug 2012 19:38:05 +0100

libio-all-perl (0.44-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Bump debhelper compatibility level to 8.

 -- gregor herrmann <gregoa@debian.org>  Thu, 06 Oct 2011 20:25:29 +0200

libio-all-perl (0.43-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Chris Butler ]
  * New upstream release
  * Update years of upstream copyright
  * Add myself to Uploaders
  * Updated Standards-Version to 3.9.2 (no changes required)

 -- Chris Butler <chrisb@debian.org>  Sat, 30 Jul 2011 12:21:03 +0200

libio-all-perl (0.41-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Prevents FTBFS with new Test::More versions
  * Remove patch (applied upstream)
  * Standards-Version 3.9.1 (no changes)
  * Add myself to Uploaders and Copyright
  * Use new 3.0 (quilt) source format
  * Refresh copyright information

  [ gregor herrmann ]
  * Add a patch to relax the regexp that checks for the build directory name
    in a test.

 -- gregor herrmann <gregoa@debian.org>  Thu, 20 Jan 2011 22:11:16 +0100

libio-all-perl (0.39-2) unstable; urgency=low

  [ Ryan Niebur ]
  * Remove Florian Ragwitz from Uploaders (Closes: #523234)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Really add debian/README.source.
  * Minimize debian/rules.
  * Add /me to Uploaders.
  * Set Standards-Version to 3.8.3 (no changes).
  * Only run test suite when localhost is available; thanks to Joey Hess for
    the bug report (closes: #537711).

 -- gregor herrmann <gregoa@debian.org>  Fri, 30 Oct 2009 17:25:19 +0100

libio-all-perl (0.39-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Rene Mayorga ]
  * New upstream release
  * debian/control
    + Use standards-version 3.8.0 instead of 3.8.0.1
    + wrap long-lines
    + add myself to uploaders
    + add quilt as build-dep
  * debian/copyright - updated
  * debian/rules - updated using dh-make-perl, quilt added
  * debian/patches
    + fix_pod_erros.patch added
      - Just remove a trailing `;' from encoding on POD
    + debian_pack_name_at_tests.patch 
      - was direct applied on .diff.gz, this change the pathname to 
        a more debian-way name.
  * debian/README.source added

 -- Rene Mayorga <rmayorga@debian.org.sv>  Fri, 12 Dec 2008 21:04:09 -0600

libio-all-perl (0.38-1) unstable; urgency=low

  [ Krzysztof Krzyżaniak (eloy) ]
  * New upstream release
  * debian/control:
   + Standards-Version upgraded to 3.8.0.1 (no changes)
   + B-D: upgraded to debhelper >= 7
   + Uploaders: added me
  * debian/compat:
   + bumped to 7 (no changes)
  * debian/copyright:
   + changed link to upstream version
  * debian/rules:
   + used /usr/share/doc/debhelper/examples/rules.simple

  [ gregor herrmann ]
  * Take over for the Debian Perl Group with maintainer's permission
    (http://lists.debian.org/debian-perl/2008/06/msg00039.html)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Florian Ragwitz
    <rafl@debian.org>); Florian Ragwitz <rafl@debian.org> moved to
    Uploaders.
  * Add debian/watch.
  
 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Wed, 18 Jun 2008 10:26:13 +0200

libio-all-perl (0.33-3.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with Perl 5.10.  Closes: #467903

 -- Mark Hymers <mhy@debian.org>  Sat, 05 Apr 2008 21:09:12 +0100

libio-all-perl (0.33-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * In t/synopsis2.t, sort the results from scanning the directories, as they
    are not specified to come back in any given order. Fixes intermittent
    FTBFS; patch backported from upstream. (Closes: #396379)

 -- Steinar H. Gunderson <sesse@debian.org>  Sun, 12 Nov 2006 19:23:29 +0100

libio-all-perl (0.33-3) unstable; urgency=low

  * Skip t/spiffy.t as it fails because of libspiffy-perl changed its
    interface (Closes: #353855).
  * Use DH_COMPAT 5.
  * Updated Maintainer address.

 -- Florian Ragwitz <rafl@debian.org>  Sun, 26 Mar 2006 21:31:20 +0200

libio-all-perl (0.33-2) unstable; urgency=low

  * New Maintainer Address.
  * Added missing libspiffy-perl dependency.

 -- Florian Ragwitz <rafl@debianforum.de>  Thu,  8 Sep 2005 13:14:45 +0200

libio-all-perl (0.33-1) unstable; urgency=low

  * Initial release (Closes: #317110).

 -- Florian Ragwitz <florian@mookooh.org>  Wed,  6 Jul 2005 13:39:16 +0200

