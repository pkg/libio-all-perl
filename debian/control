Source: libio-all-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Axel Beckert <abe@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 9~)
Build-Depends-Indep: libfile-copy-recursive-perl,
                     libfile-readbackwards-perl,
                     libmldbm-perl,
                     libspiffy-perl,
                     netbase,
                     perl
Standards-Version: 4.1.1
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libio-all-perl.git
Vcs-Git: https://anonscm.debian.org/git/pkg-perl/packages/libio-all-perl.git
Homepage: https://metacpan.org/release/IO-All

Package: libio-all-perl
Architecture: all
Depends: libspiffy-perl,
         ${misc:Depends},
         ${perl:Depends}
Suggests: libdb-file-lock-perl,
          libfile-mimeinfo-perl,
          libfile-readbackwards-perl,
          libfreezethaw-perl,
          libmldbm-perl
Description: Perl module for unified IO operations
 IO::All combines all of the best Perl IO modules into a single Spiffy object
 oriented interface to greatly simplify your everyday Perl IO idioms. It
 exports a single function called io, which returns a new IO::All object. And
 that object can do it all!
 .
 The IO::All object is a proxy for IO::File, IO::Dir, IO::Socket, IO::String,
 Tie::File, File::Spec, File::Path and File::ReadBackwards; as well as all the
 DBM and MLDBM modules. You can use most of the methods found in these classes
 and in IO::Handle (which they inherit from). IO::All adds dozens of other
 helpful idiomatic methods including file stat and manipulation functions.
